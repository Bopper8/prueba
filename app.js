  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyC5wNAJgvLKlKCkJn5Dz3jfvCxmvmJvO4w",
    authDomain: "fb-prueba-2fd69.firebaseapp.com",
    databaseURL: "https://fb-prueba-2fd69.firebaseio.com",
    projectId: "fb-prueba-2fd69",
    storageBucket: "fb-prueba-2fd69.appspot.com",
    messagingSenderId: "629205714806",
    appId: "1:629205714806:web:b41e4d8a3a102dbee7dc57"
  };
  // Initialize Firebase
 firebase.initializeApp(firebaseConfig);
 var db = firebase.database(); 
 const auth = firebase.auth();
  const fs= firebase.firestore();
//Registro

const registroForm = document.querySelector('#registroForm');//

registroForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const email = document.querySelector('#registro-email').value;
    const password = document.querySelector('#registro-password').value;
    auth
    .createUserWithEmailAndPassword(email,password)
    .then(userCredential => {
        //Se reseta el modal con la documentacion de boostrap
        registroForm.reset();
        // Y el modelo lo cerrare con la misma documentacion
        $('#modalRegistro').modal('hide')      
        alert("El registro fue exitoso")
    }).catch(function(error){
        alert("El email ya existe en la base de datos")
    });
});

//Inicio de Sesion
const iSesionForm =  document.querySelector('#iSesionForm');
iSesionForm.addEventListener('submit', e =>{
    e.preventDefault();
    const email = document.querySelector('#iSesion-email').value;
    const password = document.querySelector('#iSesion-password').value;
    auth
    .signInWithEmailAndPassword(email,password)
    .then(function(result) {
        //Se reseta el modal con la documentacion de boostrap
        iSesionForm.reset();
        // Y el modelo lo cerrare con la misma documentacion
        $('#modalISesion').modal('hide');
        document.getElementById('cSesion').style.display = 'inline';
        document.getElementById('clickSesion').style.display='none';
        document.getElementById('clickRegistro').style.display='none';  
        document.getElementById('crud').style.display='inline';    
        document.getElementById('uid').value= result.user.uid; 
        uid = result.user.uid;
        listar(uid);
        alert("Se inicio correctamente");
     }).catch(function(error){
         alert("Este email no existe en la base de datos");
     })
    });
    //Cerrar Sesion
    const cSesion = document.querySelector('#cSesion');
    cSesion.addEventListener('click', e =>{//Caputra su evento click
        e.preventDefault();//Cancelar el comportamiento por defecto
        auth.signOut().then(()=>{//sale de la authenticacion de firebase, y despues hacer...
            console.log("Se cerro la Sesion correctamente");
            document.getElementById('cSesion').style.display='none';
            document.getElementById('clickSesion').style.display='inline';
            document.getElementById('clickRegistro').style.display='inline';
            document.getElementById('crud').style.display='none';
        })
    });

//Agregar

//Agregar
auth.onAuthStateChanged(user => {
    if(user){
        const addForm =  document.querySelector('#addForm');
            addForm.addEventListener('submit', e =>{
                e.preventDefault();
                const uid = document.querySelector('#uid').value;
                const titulo = document.querySelector('#titulo').value;
                const descripcion = document.querySelector('#descripcion').value;
               fs.collection("tareas"+uid).doc().set({
                descripcion:descripcion,
                id:uid,
                titulo:titulo
               }).then(function(){
                    alert("Se agregaron los datos correctamente");
                    $('#modalAgregar').modal('hide');
                    location.reload();
               }).catch(function(error){
                   alert("Los datos no se agregaron correctamente");
               });
            });
            }else{
                alert("No se agregaron los datos")
        
            }
});

//Listar
function listar(uid){
    auth.onAuthStateChanged(user =>{
        if (user){
            const tareas = document.querySelector('.odd');
            let html ='';
            fs.collection("tareas"+uid).get().then(function(querySnapshot){
                documentos= querySnapshot.forEach(function(doc){
                        const li =`
                        <tr>
                        <th scope="col">${doc.id}</th>
                        <th scope="col">${doc.data().titulo}</td>
                        <th scope="col">${doc.data().descripcion}</td>
                        <th scope="col"><button type="button" class="btn btn-info" id="btnEditar"value="Editar">Editar</button>
                        <button type="button" class="btn btn-danger" id=btnEliminar""value="Eliminar">Eliminar</button>
                        </th></tr><br>`;
                       html +=li;

                })
                tareas.innerHTML = html;
                //publicar(snapshot.docs);
                //console.log(snapshot.docs);
            })
        }else{
           console.log("No hay datos por mostrar");
        }
    });
}
